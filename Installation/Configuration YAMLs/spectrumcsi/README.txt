Steps

1. Apply the node configurations

1. Create namespace

oc new-project ibm-spectrum-scale-ns or oc new-project ibm-spectrum-scale-ns-dmz

2. Create secret from provided credentials

oc create secret generic cnsa-remote-mount-storage-cluster-1 --from-literal=username='cnss_storage_gui_user' --from-literal=password='cnss_storage_gui_password'

3. Create resources in common directory if they do not exist

oc create -f ./common

4. Edit the files in int or dmz to suit the deployment

5. Deploy the int or dmz CNSA

oc -n ibm-spectrum-scale-ns apply -f ./int 
oc ibm-spectrum-scale-ns apply -f ./dmz

Cleanup 

Delete the Spectrum Scale CSI CR
Delete the Spectrum Scale CR

Run the following to clean up the nodes

for i in $(oc get nodes -l 'node-role.kubernetes.io/worker=','zone=int' -o jsonpath="{range .items[*]}{.metadata.name}{'\n'}"); do oc debug node/${i} -T -- chroot /host sh -c "rm -rf /var/mmfs; rm -rf /var/adm/ras"; done